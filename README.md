## REST API USER GAME BEJS CHALLENGE BINAR ACADEMY

# How to use

1. ``` npm install ```
2. ``` sequelize db:create || npx sequelize-cli db:create ```
3. ``` sequelize db:migrate || npx sequelize-cli db:migrate ```
4. ``` sequelize db:seed:all || npx sequelize-cli db:seed:all ```
5. ``` npm start ```
6. ``` Go to {{server}}/api-docs to see the Swagger API documentation . ```